package az.ingress.ms23.rest;

import az.ingress.ms23.dto.BookRequestDto;
import az.ingress.ms23.dto.BookResponseDto;
import az.ingress.ms23.service.BookService;
import az.ingress.ms23.service.BookServiceImpl;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("v1/books")
@RequiredArgsConstructor
public class BookApi {

    private final BookService bookService;


    @GetMapping("/page")
    public Page<BookResponseDto> getBookList(Pageable pageable) {
        return bookService.getPage(pageable);
    }



    @GetMapping
    public List<BookResponseDto> getBookList() {
        return bookService.get();
    }

    @GetMapping("/{id}")
    public BookResponseDto getBookById(@PathVariable Long id) {
       return bookService.getById(id);

    }

    @PostMapping
    public BookResponseDto addBook(@Validated @RequestBody BookRequestDto bookRequestDto) {
        return bookService.post(bookRequestDto);
    }

    @PutMapping("/{id}")
    public BookResponseDto updateBoook(@PathVariable Long id, @RequestBody BookRequestDto bookRequestDto) {
       return bookService.update(id,bookRequestDto);
    }

    @DeleteMapping("/{id}")
    public void deleteBook(@PathVariable Long id) {

        bookService.delete(id);

    }


}

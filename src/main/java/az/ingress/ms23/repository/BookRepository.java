package az.ingress.ms23.repository;

import az.ingress.ms23.domain.BookEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<BookEntity,Long> {

}

package az.ingress.ms23.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BookResponseDto {

        private Long id;
        private String name;
        private String authorName;
        private Long pageSize;

}

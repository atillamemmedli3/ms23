package az.ingress.ms23.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.antlr.v4.runtime.misc.NotNull;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookRequestDto {


        @NotNull
        private String name;
        @NotNull
        private String authorName;
        @NotNull
        private Long pageSize;


}

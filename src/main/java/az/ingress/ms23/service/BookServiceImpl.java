package az.ingress.ms23.service;

import az.ingress.ms23.domain.BookEntity;
import az.ingress.ms23.dto.BookRequestDto;
import az.ingress.ms23.dto.BookResponseDto;
import az.ingress.ms23.repository.BookRepository;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {

    private final ModelMapper modelMapper;
    private final BookRepository bookRepository;

    public BookServiceImpl(ModelMapper modelMapper, BookRepository bookRepository) {
        this.modelMapper = modelMapper;
        this.bookRepository = bookRepository;
    }

    @Override
    public Page<BookResponseDto> getPage(Pageable pageable) {

        return bookRepository.findAll(pageable)
                .map(bookEntity -> modelMapper.map(bookEntity, BookResponseDto.class));
    }

    @Override
    public List<BookResponseDto> get() {

        return bookRepository.findAll().stream()
                .map(bookEntity -> modelMapper.map(bookEntity, BookResponseDto.class))
                .toList();

    }

    @Override
    public BookResponseDto getById(Long id) {

        return bookRepository.findById(id)
                .map(bookEntity -> modelMapper.map(bookEntity,BookResponseDto.class))
                .orElseThrow(()-> new RuntimeException("Book Not Found"));

    }

    @Override
    public BookResponseDto post(BookRequestDto bookRequestDto) {
        BookEntity book = modelMapper.map(bookRequestDto,BookEntity.class);

        return modelMapper.map(bookRepository.save(book),BookResponseDto.class);

    }

    @Override
    public BookResponseDto update(Long id, BookRequestDto bookRequestDto) {
        final BookEntity bookEntity= modelMapper.map(bookRequestDto, BookEntity.class);
        bookEntity.setId(id);
        bookRepository.findById(id).orElseThrow(()->new RuntimeException("Book with "+id+"is not found"));
        return modelMapper.map(bookRepository.save(bookEntity),BookResponseDto.class);
    }

    @Override
    public void delete(Long id) {
        final BookEntity bookEntity= bookRepository.findById(id).orElseThrow(()->new RuntimeException("Book is not found"));

        bookRepository.delete(bookEntity);
    }
}

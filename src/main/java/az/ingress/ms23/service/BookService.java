package az.ingress.ms23.service;

import az.ingress.ms23.dto.BookRequestDto;
import az.ingress.ms23.dto.BookResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface BookService {

    Page<BookResponseDto> getPage(Pageable pageable);
    List<BookResponseDto> get();

    BookResponseDto getById(Long id);

    BookResponseDto post(BookRequestDto bookRequestDto);

    BookResponseDto update(Long id, BookRequestDto bookRequestDto);

    void delete(Long id);
}
